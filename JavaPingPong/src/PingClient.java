import App.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;

public class PingClient {
    static Pingpong pingpongReferencia;

    public static void main(String args[]) {
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef = orb
                    .resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt nameContextReferencia = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            String name = "Pingpong";
            pingpongReferencia = PingpongHelper.narrow(nameContextReferencia.resolve_str(name)); // saida do IOR

            String message = "Ping";
            String result  = pingpongReferencia.fala(message);
            System.out.println( String.format("O Cliente (Java) falou '%s'. O servidor respondeu '%s'.", message,result) );
        } catch (Exception e) {
            System.out.println("ERROR : " + e);
            e.printStackTrace(System.out);
        }
    }

}

import App.*;

import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class PingClientIor {
    static Pingpong implementacao;

    public static void main(String args[]) {
        try {
            String ior;

            // Create and initialize the ORB
            ORB orb = ORB.init(args, null);
            
            try{
            	ior = new Scanner(new File("IOR.txt")).useDelimiter("\\Z").next();
            }catch(IOException e){
            	// Tenta coletar o IOR via par�metro em �ltimo caso
            	if (args.length == 0) {
                    System.out.println("No IOR specified");
                    System.exit(-1);
                }
                ior = args[0];
            }
            
            org.omg.CORBA.Object pingpongObj = orb.string_to_object(ior);
            App.Pingpong pingpongReferencia = App.PingpongHelper.narrow(pingpongObj);
            
            String message = "Ping";
            String result  = pingpongReferencia.fala(message);
            System.out.println( String.format("O Cliente (Java IOR) falou '%s'. O servidor respondeu '%s'.", message,result) );
        } catch (Exception e) {
            System.out.println("ERROR : " + e);
            e.printStackTrace(System.out);
        }
    }

}

import App.*;

import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class PingpongImplementacao extends PingpongPOA {
    private ORB orb;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    // implementa o metodo fala(String mesg) definido no modulo .idl
    public String fala(String mesg) {
        System.out.println("O Cliente disse: " + mesg);
        return "Pong!!";
    }
}

public class PongServer {
    public static void main(String args[]) {
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get reference to rootpoa & activate the POAManager
            POA rootpoa = POAHelper.narrow(orb
                .resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            // create servant and register it with the ORB
            PingpongImplementacao implementacao = new PingpongImplementacao();
            implementacao.setORB(orb);

            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(implementacao);
            Pingpong ior = PingpongHelper.narrow(ref);

            // get the root naming context
            org.omg.CORBA.Object objRef = orb
                    .resolve_initial_references("NameService");
            // Use NamingContextExt which is part of the Interoperable
            // Naming Service (INS) specification.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // bind the Object Reference in Naming
            NameComponent[] path = new NameComponent[1];
            path[0] = new NameComponent ("Pingpong", "");

            ncRef.rebind(path, ior);
            
            // tenta gerar o IOR em um arquivo para facilitar
            try {
            	File file = new File("IOR.txt");
            	
            	// if file doesnt exists, then create it
    			//if (!file.exists()) {
    				file.createNewFile();
    			//}
     
    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
    			BufferedWriter bw = new BufferedWriter(fw);
    			bw.write(ior.toString());
    			bw.close();
    			
            } catch (IOException e) {
    			//e.printStackTrace();
    			// Gera o IOR no console em ultimo caso
    			System.out.println( ior.toString() ); // IOR
    		}

            System.out.println("PongServer [IOR] ready and waiting ...");

            // wait for invocations from clients
            orb.run();
        } catch (Exception e) {
            System.out.println("ERROR : " + e);
            e.printStackTrace(System.out);
        }

        System.out.println("PongServer [IOR] Exiting ...");
    }
}
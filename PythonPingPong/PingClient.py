#!/usr/bin/env python
#
# Detecção automática do NameServer (identificação do servidor com a aplicação)
#
import sys
from omniORB import CORBA
import CosNaming, App

# Initialise the ORB
orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)

# Obtain a reference to the root naming context
objRef = orb.resolve_initial_references("NameService")
rootContext = objRef._narrow(CosNaming.NamingContext)

if rootContext is None:
    print "Failed to narrow the root naming context"
    sys.exit(1)

# Resolve the name "test.my_context/AppPingpong.Object"
name = [CosNaming.NameComponent("Pingpong", ""), CosNaming.NameComponent("AppPingpong", "")]
# name = [CosNaming.NameComponent("test", "my_context"), CosNaming.NameComponent("AppPingpong", "Object")]
# name = [CosNaming.NameComponent("Pingpong", "")]

try:
    objRef = rootContext.resolve(name)
except CosNaming.NamingContext.NotFound, ex:
    print "Name not found"
    sys.exit(1)

# Narrow the object to an App::Pingpong
pingpongReferencia = objRef._narrow(App.Pingpong)

if pingpongReferencia is None:
    print "Object reference is not an App::Pingpong"
    sys.exit(1)

# Invoke the fala operation
message = "Ping"
result  = pingpongReferencia.fala(message)

print "O Cliente (Python) falou '%s'. O servidor respondeu '%s'." % (message,result)
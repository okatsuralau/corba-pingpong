#!/usr/bin/env python

import sys
from omniORB import CORBA
import App

orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)

# coleta automaticametne o IOR gerado pelo server
try:
    file = open('IOR.txt', 'r')
    ior  = file.read()
except IOError:
    # coleta o IOR dos parametros passados pela linha de comando
    ior = sys.argv[1]

    if not ior:
        print "O arquivo IOR.txt nao foi gerado ou a string IOR nao foi informada. Inicie o servidor primeiro."
        sys.exit(1)


pingpongObj        = orb.string_to_object(ior)
pingpongReferencia = pingpongObj._narrow(App.Pingpong)
if pingpongReferencia is None:
    print "Object reference is not an App::Pingpong"
    sys.exit(1)

message = "Ping"
result  = pingpongReferencia.fala(message)

print "O Cliente (Python) (IOR) falou '%s'. O servidor respondeu '%s'." % (message,result)
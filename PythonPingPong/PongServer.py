#!/usr/bin/env python

import sys
from omniORB import CORBA, PortableServer
import CosNaming, App, App__POA

# Define a href da interface Pingpong
class PongServer (App__POA.Pingpong):
    def fala(self, mesg):
        print "O cliente disse: ", mesg
        return "Pong!!";

# Initialise the ORB and find the root POA
# sys.argv.extend(("-ORBInitRef", "NameService=corbaname::localhost"))
orb     = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
rootpoa = orb.resolve_initial_references("RootPOA")

# Create an instance of PongServer and an Pingpong object reference
implementacao = PongServer()
href          = implementacao._this()

# Obtain a reference to the root naming context
objRef      = orb.resolve_initial_references("NameService")
rootContext = objRef._narrow(CosNaming.NamingContext)

if rootContext is None:
    print "Failed to narrow the root naming context"
    sys.exit(1)

# Bind a context named "Pingpong" to the root context
name = [CosNaming.NameComponent("Pingpong", "")]
try:
    PingpongContext = rootContext.bind_new_context(name)
    print "New Pingpong context bound"

except CosNaming.NamingContext.AlreadyBound, ex:
    print "Pingpong context already exists"
    objRef = rootContext.resolve(name)
    PingpongContext = objRef._narrow(CosNaming.NamingContext)
    if PingpongContext is None:
        print "Pingpong.mycontext exists but is not a NamingContext"
        sys.exit(1)

# Bind the Pingpong object to the Pingpong context
name = [CosNaming.NameComponent("AppPingpong", "")]
try:
    PingpongContext.bind(name, href)
    print "New AppPingpong object bound"

except CosNaming.NamingContext.AlreadyBound:
    PingpongContext.rebind(name, href)
    print "AppPingpong binding already existed -- rebound"

print "PongServer ready and waiting ..."

# Activate the POA
poaManager = rootpoa._get_the_POAManager()
poaManager.activate()

# Block for ever (or until the ORB is shut down)
orb.run()
#!/usr/bin/env python

import sys
from omniORB import CORBA, PortableServer
import App, App__POA

class PongServerIor (App__POA.Pingpong):
    def fala(self, mesg):
        print "O cliente disse: ", mesg
        return "Pong!!";

orb     = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
rootpoa = orb.resolve_initial_references("RootPOA")

implementacao = PongServerIor()
href          = implementacao._this()

# print orb.object_to_string(href) # impressao do IOR na linha de comando

# cria o IOR para que o cliente possa se comunicar com este server
file = open("IOR.txt", "w")
file.write(orb.object_to_string(href))
file.close()

print "\n\nPongServer [IOR] ready and waiting ..."

poaManager = rootpoa._get_the_POAManager()
poaManager.activate()

orb.run()
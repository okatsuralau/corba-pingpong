# CORBA Ping Pong

Exercício de comunicação entre aplicações distribuídas escritas em linguagens distintas a partir de um mesmo código-fonte (IDL).



### Requisitos
- Java (mais recente)
- Python 4.2
- [omniORB](http://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.0/)
- [omniORBpy](http://sourceforge.net/projects/omniorb/files/omniORBpy/omniORBpy-4.2.0/)



### Configurações

Depois de instalar o Python e baixar os pacotes do omniORB (os binários de preferência)(win32...) é necessário adicionar os pacotes às variáveis de ambiente do Windows.

- Crie a variável **OMNIORB** com valor: **D:\omniORB-4.2.0** *(pode ser outro diretório de sua escolha)*;
- Crie a variável **OMNIORBPY** com valor: **D:\omniORBpy-4.2.0** *(pode ser outro diretório de sua escolha)*;
- Crie a variável **OMNINAMES_LOGDIR** com valor: **%OMNIORB%\data** para armazenar dados de compilação;
- Adicione as bibliotecas do OmniORB ao path: **%OMNIORB%\bin\x86_win32;%OMNIORBPY%\bin\x86_win32;**;
- Inclua, também, as bibliotecas do Python ao path, para facilitar a compilação: **D:\Python27;** *(mude para o diretório de sua instalação)*;

- Pode ser útil instalar, também, as variáveis **PYTHONHOME** com o diretório raís de sua instalação e o **PYTHONPATH**.
Este último, com os valores: **%OMNIORB%\lib\python;%OMNIORB%\lib\x86_win32;%OMNIORBPY%\lib\python;%OMNIORBPY%\lib\x86_win32;%PYTHONHOME%;%PYTHNHOME%\DLLs;%PYTHONHOME%\Lib**

> **NOTA**
>
> Os endereços do path, no windows, são separados por ponto-e-vírgula (*;*) menos o último da string.

### Guias de teste
- [JavaServer + JavaClient](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/JavaServerJavaClient)
- [JavaServer + JavaClient (IOR)](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/JavaServerJavaClient_IOR)
- [PythonServer + PythonClient](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/PythonServerPythonClient)
- [PythonServer + PythonClient (IOR)](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/PythonServerPythonClient_IOR)
- [JavaServer + PythonClient (IOR)](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/JavaServerPythonClient_IOR)
- [PythonServer + JavaClient (IOR)](https://bitbucket.org/okatsuralau/corba-pingpong/wiki/PythonServerJavaClient_IOR)


### Links de referência
#### Corba + JAVA
- http://docs.oracle.com/javase/7/docs/technotes/guides/idl/
- http://www.ejbtutorial.com/programming/tutorial-for-corba-hello-world-using-java
- http://www.das.ufsc.br/~fabio/java-corba/tutorial.pdf
- http://www.inf.ufsc.br/~frank/INE5418/CORBA-Banco/index.htm
- http://www.cin.ufpe.br/~sd/disciplinas/sd/pos/aulas/implementacao_corba.htm
- http://isp.vsi.ru/library/Java/JExpSol/ch18.htm
- http://legionti.blogspot.com.br/2010/11/descricao-essa-atividade-mostrara-como.html
- http://www.omniorb-support.com/pipermail/omniorb-list/2001-July/018775.html
- http://elib.dlr.de/59394/1/Mixing_Python_and_Java.pdf
- http://docs.oracle.com/cd/A87860_01/doc/java.817/a83722/appcorb5.htm
- http://www.gta.ufrj.br/grad/00_2/corba/
- http://www.inf.ufrgs.br/~johann/sisop2/GVGOgrupo2.htm

#### CORBA + Python
- http://omniorb.sourceforge.net/omnipy42/omniORBpy/omniORBpy002.html#toc3
- http://omniorb.sourceforge.net/omnipy42/omniORBpy/omniORBpy007.html#toc40
- http://www.bioinformatics.org/bradstuff/bc/IntroPythonServer.pdf
- http://www.bioinformatics.org/bradstuff/bc/IntroPythonClient.pdf
- http://www.javaworld.com/article/2076339/core-java/locating-corba-objects-using-java-idl.html
- http://www.javaworld.com/article/2076339/core-java/locating-corba-objects-using-java-idl.html?page=2 (pode ter a resposta do problema de detectar o NameServer entre Python e Java)
- http://code.activestate.com/recipes/81254-implement-a-corba-client-and-server/

#### Mão na massa!!!